// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript?
// По цепочке передаются свойства и методы к дочерним обьектам.

// Для чого потрібно викликати super() у конструкторі класу-нащадка?
// У классов может быть лишь один конструктор. Чтобы вызвать конструктор у дочернего класса нужно сперва прописать super(), а дальше this для дочернего класса. Иначе this работать не будет.

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  getName() {
    return this.name;
  }
  setName(name) {
    this.name = name;
  }
  getAge() {
    return this.age;
  }
  setAge(age) {
    this.age = age;
  }
  getSalary() {
    return this.salary;
  }
  setSalary(name) {
    this.salary = salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  getSalary() {
    return this.salary * 3;
  }
  getLang() {
    return this.lang;
  }
}

const programmer1 = new Programmer("John", 32, 2000, ["ololo"]);

const programmer2 = new Programmer("Mike", 23, 3000, ["kekeke"]);

const programmer3 = new Programmer("Bob", 45, 4000, ["fefefe"]);

console.log(
  programmer2,
  programmer2.getSalary(),
  programmer2.getAge(),
  programmer2.getName(),
  programmer2.getLang()
);
console.log(
  programmer1,
  programmer1.getSalary(),
  programmer1.getAge(),
  programmer1.getName(),
  programmer1.getLang()
);
console.log(
  programmer3,
  programmer3.getSalary(),
  programmer3.getAge(),
  programmer3.getName(),
  programmer3.getLang()
);
